# Translation of _Man Into Woman_ Transcription
  
  Permission to use these materials was granted by the archive's primary editor, Dr. Pamela Caughie. **Students and community members are not permitted to share or modify these materials without first gaining permission from Dr. Caughie directly.**  
    
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>  
  
The files in this folder are attributed to the <a xmlns:cc="http://creativecommons.org/ns#" href="http://lilielbe.org/" property="cc:attributionName" rel="cc:attributionURL">http://lilielbe.org/</a> and are licensed under a    
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License</a>.  
  
related schema and encoding guidelines - https://gitlab.com/ctsdh/lili-elbe-code  
  
Transcription Repo. - TS - https://gitlab.com/ctsdh/lilielbe_TS  
